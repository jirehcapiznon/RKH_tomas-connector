'use strict'

const Reekoh = require('reekoh')
const plugin = new Reekoh.plugins.Connector()
/* eslint-disable new-cap */
const rkhLogger = new Reekoh.logger('tomas-connector')
const request = require('request-promise')
const get = require('lodash.get')
const xml2js = require('xml2js')
const builder = new xml2js.Builder()
const moment = require('moment')

let parseString = require('xml2js').parseString
let isEmpty = require('lodash.isempty')

let payloadTemplate
let doRead
let updatePothole
let result
let options

plugin.on('data', (data) => {
  let ts = plugin.processStart()
  let lat = get(data, 'latitude')
  let lng = get(data, 'longitude')
  let classification = get(data, 'classification')
  let dateMoment = get(data, 'dateMoment')
  let assetNumber = get(data, 'AssetNumber')
  let detectedDate = get(data, 'detected')
  let potholeId = get(data, 'pothole_id')
  let severity = get(data, 'severity') || 0
  let type = get(data, 'type') || ''
  let lastSeen = get(data, 'last_seen')
  let passed = get(data, 'passed') || 0
  let missed = get(data, 'missed') || 0
  let defectCode = get(data, 'defect_code') || 'AI.POTHOLE'

  if (isEmpty(type)) {
    plugin.processDone(ts)
    return plugin.logException(new Error('Invalid Data. Type cannot be empty'))
  }

  if (type !== 'new' && type !== 'updated' && type !== 'repaired' && type !== 'fixed') {
    plugin.processDone(ts)
    return plugin.logException(new Error('Invalid Type. Only "Type": new, updated, repaired or fixed'))
  }

  if (isEmpty(assetNumber)) {
    plugin.processDone(ts)
    return plugin.logException(new Error('Invalid Data. AssetNumber cannot be empty'))
  }

  if (type === 'new') {
    console.log('creating of new potholes')
    // creating of new potholes
    payloadTemplate = {
      's:Envelope': {
        '$': {
          'xmlns:s': 'http://www.w3.org/2003/05/soap-envelope'
        },
        's:Header': [
          ''
        ],
        's:Body': [
          {
            '$': {
              'xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
              'xmlns:xsd': 'http://www.w3.org/2001/XMLSchema'
            },
            'DefectInstance_DoCreate': [
              {
                '$': {
                  'xmlns': 'http://TechnologyOneCorp.com/T1.W1.Public/Services'
                },
                'Request': [
                  {
                    '$': {
                      'PerformSaveAttachments': 'false'
                    },
                    'Auth': [
                      {
                        '$': {
                          'UserId': `${plugin.config.username}`,
                          'Password': `${plugin.config.password}`,
                          'Config': `${plugin.config.config}`,
                          'FunctionName': '$W1.DEFINST.CREAT.WS'
                        }
                      }
                    ],
                    'DefectInstances': [
                      {
                        'DefectInstances': [
                          {
                            '$': {
                              'DefectTime': `${dateMoment}`,
                              'DefectDate': `${dateMoment}`,
                              'Source': 'IM',
                              'Status': 'I',
                              'State': 'Added',
                              'DefectCode': `${defectCode}`,
                              'AssetNumber': `${assetNumber}`,
                              'RegisterName': 'MBRCOP',
                              'Stage': `R`
                            },
                            'Attachments': [
                              ''
                            ],
                            'Geographies': {
                              'Geographies': [
                                {
                                  '$': {
                                    'ElementType': '1',
                                    'Layer': 'Defect',
                                    'State': 'Added'
                                  },
                                  'Points': [
                                    {
                                      '$': {
                                        'Latitude': `${lat}`,
                                        'Longitude': `${lng}`
                                      }
                                    }
                                  ]
                                }
                              ]
                            },
                            'CustomFieldValues': { // CustomFieldValues element attribute of the defect instance. Note: use of array to create subelements.
                              'CustomFieldValues': [
                                { '$': { 'xsi:nil': 'true' } },
                                { '$': { 'xsi:nil': 'true' } },
                                { '$': { 'xsi:nil': 'true' } },
                                { '$': { 'xsi:nil': 'true' } },
                                { '$': { 'xsi:nil': 'true' } },
                                { '$': { 'xsi:nil': 'true' } },
                                { '$': { 'xsi:nil': 'true' } },
                                { '$': { 'xsi:nil': 'true' } },
                                { '$': { 'xsi:nil': 'true' } },
                                { '$': { 'xsi:nil': 'true' } },
                                { '$': { 'xsi:nil': 'true' } },
                                { '$': { 'xsi:nil': 'true' } },
                                { '$': { 'xsi:nil': 'true' } },
                                { '$': { 'xsi:nil': 'true' } },
                                { '$': { 'xsi:nil': 'true' } },
                                { '$': { 'xsi:nil': 'true' } },
                                { '$': { 'xsi:nil': 'true' } },
                                { '$': { 'xsi:nil': 'true' } },
                                { '$': { 'xsi:nil': 'true' } },
                                { '$': { 'xsi:nil': 'true' } }
                              ]
                            },
                            'DefectCodeCustomFieldValues': [
                              {

                                'DefectCodeCustomFieldValues': [
                                  {
                                    '$': {
                                      'ValAlpha': `${potholeId}`
                                    }
                                  }, {
                                    '$': {
                                      'ValNum': `${severity}`
                                    }
                                  }, {
                                    '$': {
                                      'ValAlpha': `${type}`
                                    }
                                  }, {
                                    '$': {
                                      'ValDateTime': `${lastSeen}`
                                    }
                                  }, {
                                    '$': {
                                      'ValNum': `${passed}`
                                    }
                                  }, {
                                    '$': {
                                      'ValNum': `${missed}`
                                    }
                                  }, {
                                    '$': {
                                      'ValDateTime': `${detectedDate}`
                                    }
                                  },
                                  { '$': { 'xsi:nil': 'true' } },
                                  { '$': { 'xsi:nil': 'true' } },
                                  { '$': { 'xsi:nil': 'true' } },
                                  { '$': { 'xsi:nil': 'true' } },
                                  { '$': { 'xsi:nil': 'true' } },
                                  { '$': { 'xsi:nil': 'true' } },
                                  { '$': { 'xsi:nil': 'true' } },
                                  { '$': { 'xsi:nil': 'true' } },
                                  { '$': { 'xsi:nil': 'true' } },
                                  { '$': { 'xsi:nil': 'true' } },
                                  { '$': { 'xsi:nil': 'true' } },
                                  { '$': { 'xsi:nil': 'true' } }
                                ]
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      }
    }

    result = builder.buildObject(payloadTemplate)

    options = {
      method: 'POST',
      uri: plugin.config.url,
      headers: {
        'Content-Length': Buffer.byteLength(result),
        'SOAPAction': 'http://TechnologyOneCorp.com/T1.W1.Public/Services/DefectInstance_DoCreate',
        'Content-Type': 'text/xml',
        'Accept-Charset': 'utf-8',
        'cache-control': 'no-cache'
      },
      body: result,
      json: false
    }

    return request(options)
      .then((response) => {
        console.log('response', response)
        return parseString(response, (err, result) => {
          if (err) {
            console.log(err)
          }

          let handleError = result['soap:Envelope']['soap:Body'][0]['DefectInstance_DoCreateResponse'][0]['DefectInstance_DoCreateResult'][0]['Errors'][0]['$']['IsError']

          if (handleError === 'false') {
            plugin.processDone(ts)
            return plugin.log({
              title: 'Data Received - Tomas Connector',
              message: 'Creating of Potholes',
              data: data,
              response: response
            })
          } else {
            return plugin.logException(new Error(response))
          }
        })
      })
      .catch((err) => {
        console.log(new Error(err))
        plugin.processDone(ts)
        return plugin.logException(new Error(err))
      })
  } else if (type === 'repaired' || type === 'updated' || type === 'fixed') {
    // updating or closing of potholes
    console.log('updating of new potholes')
    doRead = {
      's:Envelope': {
        '$': {
          'xmlns:s': 'http://schemas.xmlsoap.org/soap/envelope/'
        },
        's:Header': [
          ''
        ],
        's:Body': [
          {
            '$': {
              'xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
              'xmlns:xsd': 'http://www.w3.org/2001/XMLSchema'
            },
            'DefectInstance_DoRead': [
              {
                '$': {
                  'xmlns': 'http://TechnologyOneCorp.com/T1.W1.Public/Services'
                },
                'Request': [
                  {
                    '$': {
                      'RegName': 'MBRCOP',
                      'AssetNumberi': `${assetNumber}`,
                      'PerformReadAttachments': 'false'
                    },
                    'Auth': [
                      {
                        '$': {
                          'UserId': `${plugin.config.username}`,
                          'Password': `${plugin.config.password}`,
                          'Config': `${plugin.config.config}`,
                          'FunctionName': '$W1.DEFINST.READ.WS'
                        }
                      }
                    ],
                    'Criteria': [
                      {
                        'critSpec': [
                          {
                            '$': {
                              'OperatorCode': `=`,
                              'PropertyName': `DefectCodeCustomFieldValue1`,
                              'Value': `${potholeId}`
                            }
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      }
    }

    result = builder.buildObject(doRead)

    options = {
      method: 'POST',
      uri: plugin.config.url,
      headers: {
        'Content-Length': Buffer.byteLength(result),
        'SOAPAction': `http://TechnologyOneCorp.com/T1.W1.Public/Services/DefectInstance_DoRead`,
        'Content-Type': 'text/xml',
        'Accept-Charset': 'utf-8',
        'cache-control': 'no-cache'
      },
      body: result,
      json: false
    }
    return request(options)
      .then((response) => {
        const opts = {
          mergeAttrs: true,
          attrNameProcessors: [str => '@' + str]
        }

        xml2js.parseString(response, opts, (err, res) => {
          if (err) {
            return plugin.logException(new Error(err))
          }
          let defectUniqueId = res['soap:Envelope']['soap:Body'][0]['DefectInstance_DoReadResponse'][0]['DefectInstance_DoReadResult'][0]['DefectInstances'][0]['DefectInstances'][0]['@DefectUniqueId'][0]
          console.log('defectUnique', defectUniqueId)

          if (isEmpty(defectUniqueId)) {
            return plugin.logException(new Error('Invalid DefectUniqueId. DefectUniqueId could not be empty.'))
          }
          let defectTime = moment(detectedDate, 'YYYY-MM-DD[T]HH:mm:ss').format('HH:mm:ss A')
          let defectDate = moment(detectedDate, 'YYYY-MM-DD[T]HH:mm:ss').format('DD-MMM-YYYY')

          if (type === 'repaired' || type === 'fixed') {
            console.log('closing of potholes')
            let complTime = moment(lastSeen, 'YYYY-MM-DD[T]HH:mm:ss').format('HH:mm:ss A')
            let complDate = moment(lastSeen, 'YYYY-MM-DD[T]HH:mm:ss').format('DD/MM/YYYY')

            updatePothole = {
              's:Envelope': {
                '$': {
                  'xmlns:s': 'http://schemas.xmlsoap.org/soap/envelope/'
                },
                's:Header': [
                  ''
                ],
                's:Body': [
                  {
                    '$': {
                      'xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
                      'xmlns:xsd': 'http://www.w3.org/2001/XMLSchema'
                    },
                    'DefectInstance_DoUpdate': [
                      {
                        '$': {
                          'xmlns': 'http://TechnologyOneCorp.com/T1.W1.Public/Services'
                        },
                        'Request': [
                          {
                            '$': {
                              'PerformSaveAttachments': 'false'
                            },
                            'Auth': [
                              {
                                '$': {
                                  'UserId': `${plugin.config.username}`,
                                  'Password': `${plugin.config.password}`,
                                  'Config': `${plugin.config.config}`,
                                  'FunctionName': '$W1.DEFINST.UPDAT.WS'
                                }
                              }
                            ],
                            'DefectInstances': [
                              {
                                'DefectInstances': [
                                  {
                                    '$': {
                                      'ComplTime': `${complTime}`,
                                      'ComplDate': `${complDate}`,
                                      'DefectTime': `${defectTime}`,
                                      'DefectDate': `${defectDate}`,
                                      'Source': 'IM',
                                      'Status': `I`,
                                      'State': 'Modified',
                                      'DefectUniqueId': `${defectUniqueId}`,
                                      'DefectCode': `${defectCode}`,
                                      'AssetNumber': `${assetNumber}`,
                                      'RegisterName': 'MBRCOP',
                                      'Stage': `W`
                                    },
                                    'CustomFieldValues': { // CustomFieldValues element attribute of the defect instance. Note: use of array to create subelements.
                                    },
                                    'DefectCodeCustomFieldValues': [
                                      {

                                        'DefectCodeCustomFieldValues': [
                                          {
                                            '$': {
                                              'ValAlpha': `${potholeId}`
                                            }
                                          }, {
                                            '$': {
                                              'ValNum': `${severity}`
                                            }
                                          }, {
                                            '$': {
                                              'ValAlpha': `${type}`
                                            }
                                          }, {
                                            '$': {
                                              'ValDateTime': `${lastSeen}`
                                            }
                                          }, {
                                            '$': {
                                              'ValNum': `${passed}`
                                            }
                                          }, {
                                            '$': {
                                              'ValNum': `${missed}`
                                            }
                                          }, {
                                            '$': {
                                              'ValDateTime': `${detectedDate}`
                                            }
                                          }, {
                                            '$': {
                                              'ValAlpha': `${classification}`
                                            }
                                          }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            }

            let resultUpdate = builder.buildObject(updatePothole)

            let optionsUpdates = {
              method: 'POST',
              uri: plugin.config.url,
              headers: {
                'Content-Length': Buffer.byteLength(resultUpdate),
                'SOAPAction': `http://TechnologyOneCorp.com/T1.W1.Public/Services/DefectInstance_DoUpdate`,
                'Content-Type': 'text/xml',
                'Accept-Charset': 'utf-8',
                'cache-control': 'no-cache'
              },
              body: resultUpdate,
              json: false
            }

            return request(optionsUpdates)
              .then((response) => {
                plugin.processDone(ts)
                return plugin.log({
                  title: 'Data Received - Tomas Connector',
                  message: 'Closing of Potholes',
                  data: data,
                  response: response
                })
              })
              .catch((err) => {
                console.log('catch--------------------------', err)
                plugin.processDone(ts)
                return plugin.logException(new Error(err))
              })
          } else if (type === 'updated') {
            console.log('updating of Potholes')
            updatePothole = {
              's:Envelope': {
                '$': {
                  'xmlns:s': 'http://schemas.xmlsoap.org/soap/envelope/'
                },
                's:Header': [
                  ''
                ],
                's:Body': [
                  {
                    '$': {
                      'xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
                      'xmlns:xsd': 'http://www.w3.org/2001/XMLSchema'
                    },
                    'DefectInstance_DoUpdate': [
                      {
                        '$': {
                          'xmlns': 'http://TechnologyOneCorp.com/T1.W1.Public/Services'
                        },
                        'Request': [
                          {
                            '$': {
                              'PerformSaveAttachments': 'false'
                            },
                            'Auth': [
                              {
                                '$': {
                                  'UserId': `REEKOHWSUSER`,
                                  'Password': `kl34!56Vs2`,
                                  'Config': `MBRC-TEST-CES direct`,
                                  'FunctionName': '$W1.DEFINST.UPDAT.WS'
                                }
                              }
                            ],
                            'DefectInstances': [
                              {
                                'DefectInstances': [
                                  {
                                    '$': {
                                      'DefectTime': `${defectTime}`,
                                      'DefectDate': `${defectDate}`,
                                      'Source': `IM`,
                                      'Status': `A`,
                                      'State': 'Modified',
                                      'DefectUniqueId': `${defectUniqueId}`,
                                      'DefectCode': `${defectCode}`,
                                      'AssetNumber': `${assetNumber}`,
                                      'RegisterName': `MBRCOP`,
                                      'Stage': `R`
                                    },
                                    'CustomFieldValues': { // CustomFieldValues element attribute of the defect instance. Note: use of array to create subelements.
                                    },
                                    'DefectCodeCustomFieldValues': [
                                      {

                                        'DefectCodeCustomFieldValues': [
                                          {
                                            '$': {
                                              'ValAlpha': `${potholeId}`
                                            }
                                          }, {
                                            '$': {
                                              'ValNum': `${severity}`
                                            }
                                          }, {
                                            '$': {
                                              'ValAlpha': `${type}`
                                            }
                                          }, {
                                            '$': {
                                              'ValDateTime': `${lastSeen}`
                                            }
                                          }, {
                                            '$': {
                                              'ValNum': `${passed}`
                                            }
                                          }, {
                                            '$': {
                                              'ValNum': `${missed}`
                                            }
                                          }, {
                                            '$': {
                                              'ValDateTime': `${detectedDate}`
                                            }
                                          }, {
                                            '$': {
                                              'ValAlpha': `${classification}`
                                            }
                                          }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            }
          }

          let resultUpdate = builder.buildObject(updatePothole)

          let optionsUpdates = {
            method: 'POST',
            uri: plugin.config.url,
            headers: {
              'Content-Length': Buffer.byteLength(resultUpdate),
              'SOAPAction': `http://TechnologyOneCorp.com/T1.W1.Public/Services/DefectInstance_DoUpdate`,
              'Content-Type': 'text/xml',
              'Accept-Charset': 'utf-8',
              'cache-control': 'no-cache'
            },
            body: resultUpdate,
            json: false
          }

          return request(optionsUpdates)
            .then((response) => {
              plugin.processDone(ts)
              return plugin.log({
                title: 'Data Received - Tomas Connector',
                message: 'Updating of Potholes',
                data: data,
                response: response
              })
            })
            .catch((err) => {
              console.log('catch--------------------------', err)
              plugin.processDone(ts)
              return plugin.logException(new Error('Invalid Data. Error in Updating of Potholes.'))
            })
        })
      })
      .catch((err) => {
        console.log('catch-------------------------- response', err)
        plugin.processDone(ts)
        return plugin.logException(new Error('Invalid Data. Error in Updating of Potholes.'))
      })
  } else {
    return plugin.logException(new Error('Invalid Data Type. Type should be new, updated or repaired.'))
  }
})

plugin.once('ready', () => {
  console.log('tomas connector')
  plugin.log('Tomas Connector has been initialized.')
  rkhLogger.info('Tomas Connector has been initialized.')
  plugin.emit('init')
})

module.exports = plugin
