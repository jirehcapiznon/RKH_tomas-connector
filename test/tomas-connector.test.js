/* global describe, after, before, it */
'use strict'

const amqp = require('amqplib')

let _channel = null
let _conn = null
let app = null

describe('Tomas Connector Test', () => {
  before('init', () => {
    process.env.BROKER = 'amqp://guest:guest@localhost'
    process.env.INPUT_PIPE = 'ip.tomas'
    process.env.LOGGERS = 'logger1'
    process.env.EXCEPTION_LOGGERS = 'ex.logger1,ex.logger2'
    process.env.CONFIG = '{"url": "https://mbrc-test.t1cloud.com/T1CESDefault/App/W1_Services.asmx", "username": "REEKOHWSUSER", "password": "kl34!56Vs2", "config": "MBRC-TEST-CES direct"}'

    amqp.connect(process.env.BROKER)
      .then((conn) => {
        _conn = conn
        return conn.createChannel()
      }).then((channel) => {
        _channel = channel
      }).catch((err) => {
        console.log(err)
      })
  })

  after('close connection', function (done) {
    _conn.close()
    done()
  })

  describe('#start', function () {
    it('should start the app', function (done) {
      this.timeout(10000)
      app = require('../app')
      app.once('init', done)
    })
  })

  describe('#data', () => {
    it('should send data to third party client', function (done) {
      this.timeout(15000)

      let data = {
        // 'pothole_id': '3a114b7c-3bcb-11e9-b210-d663bd873d93',
        // 'severity': 1,
        // 'type': 'new',
        // 'last_seen': '2019-03-01T08:23:52',
        // 'passed': 1,
        // 'missed': 0,
        // 'detected': '2019-03-01T08:23:52',
        // // 'detected': '20180701112354'
        // 'latitude': '-27.17729599999999834',
        // 'dateMoment': '01/07/2018 11:23 AM',
        // 'longitude': '152.97252299999999536',
        // 'classification': 'fixed',
        // 'AssetNumber': 'A00559595'

        // "pothole_id": "5b2feeaf-3a41-44fb-a1a5-678ba86542fa",
        // "severity": 2,
        // "type": "new",
        // "last_seen": "2019-05-27T13:57:24",
        // "passed": 1,
        // "missed": 0,
        // "dateMoment": "27/05/2019 01:57 PM",
        // "detected": "2019-05-27T13:57:24",
        // "latitude": "-27.160206",
        // "longitude": "153.003403",
        // "classification": "pothole_small",
        // "AssetNumber": "A00558323"
      }

      _channel.sendToQueue('ip.tomas', Buffer.from(JSON.stringify(data)))
      setTimeout(done, 10000)
    })
  })
})
