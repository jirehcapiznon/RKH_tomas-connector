# TOMAS (Technology One Asset Management System) Connector

## Description

The TOMAS Connector Plugin sends data to a Technology One Asset Management System endpoint.

## Configuration

The TOMAS Connector Plugin configuration can be done once you've created your own pipeline in Reekoh. To configure the plugin, you will be asked to provide the following details:

- **Connector Name** - This is a label given to your plugin to locate it easily in your pipeline.
- **URL** - TOMAS URL Endpoint.
- **Username** - TOMAS Authentication Username.
- **Password** - TOMAS Authentication Password.
- **TOMAS Config** - TOMAS Authentication config.

![](https://reekohdocs.blob.core.windows.net/plugin-docs/connector/tomas-connector/1.0.0/tomas-config.png)

## Send Data

In order to simulate sending data, You will need a Gateway Plugin to send the data to TOMAS Connector Plugin. In the screenshot below, it uses HTTP Gateway Plugin. Note: Look for the documentation on how to use this plugin.

![](https://reekohdocs.blob.core.windows.net/plugin-docs/connector/tomas-connector/1.0.0/tomas-pipeline1.png)

Make sure your plugins and pipeline are successfully deployed.

Using **POST MAN** as HTTP Client simulator, you can now simulate sending data.

![](https://reekohdocs.blob.core.windows.net/plugin-docs/connector/tomas-connector/1.0.0/tomas-postman.png)

## Data format

The HTTP Gateway Plugin accepts application/json, application/x-www-form-urlencoded and multipart/form-data formats. Also, a "device" field is required to be in the request body. This device field should contain a device ID which is registered in Reekoh's Device Registry. Note: You need a converter to convert the following data into TOMAS Connector.

## Sample input data

```
{
  "device": "XDevice",
  "AssetNumber": "A00562984",
  "pothole_id": "3a114cda-3bcb-11e9-b210-d663bd873d93",
  "latitude": "-27.1717240000000011",
  "longitude": "152.98217099999999391",
  "classification": "pothole_small",
  "severity": "2",
  "type": "updated",
  "detected": "20180701112354",
  "last_seen": "20190301082326",
  "passed": "18",
  "missed": "1"
}
```

## Sample Converter Code

```
{
'use strict'

exports.handle = function (data) {
  const _ = require('lodash')
  const moment = require('moment')
  let date = _.get(data, 'detected')
  let lastSeen = _.get(data, 'last_seen')
  let dateFormat = moment(date, 'YYYYMMDDhhmmss')
  let finalDate = dateFormat.format('YYYY-MM-DD[T]HH:mm:ss')
  
  let result = {
    'pothole_id': _.get(data, 'pothole_id'),
    'severity': parseInt(_.get(data, 'severity')),
    'type': _.get(data, 'type'),
    'last_seen': moment(lastSeen, 'YYYYMMDDhhmmss').format('YYYY-MM-DD[T]HH:mm:ss'),
    'passed': parseInt(_.get(data, 'passed')),
    'missed': parseInt(_.get(data, 'missed')),
    'detected': finalDate,
    'latitude': _.get(data, 'latitude'),
    'dateMoment': moment(finalDate, 'YYYY-MM-DD[T]HH:mm:ss').format('DD/MM/YYYY hh:mm A'),
    'longitude': _.get(data, 'longitude'),
    'classification': _.get(data, 'classification'),
    'AssetNumber': _.get(data, 'AssetNumber')
  }
  return Promise.resolve(result)
}
```

## Verify Data

The device data will be ingested by the HTTP Gateway plugin, which will be forwarded to all the other plugins that are connected to it in the pipeline.

To verify if the data is ingested properly in the pipeline, you need to check the **LOGS** tab in every plugin in the pipeline. All the data that have passed through in the plugin will be logged in the **LOGS** tab.
If an error occurs, an error exception will be logged in the **EXCEPTIONS** tab.

![](https://reekohdocs.blob.core.windows.net/plugin-docs/connector/tomas-connector/1.0.0/tomas-logs.png)