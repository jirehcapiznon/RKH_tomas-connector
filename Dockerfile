FROM node:boron

MAINTAINER Reekoh

COPY . /home/node/tomas-connector

WORKDIR /home/node/tomas-connector

# RUN npm install pm2@2.6.1 -g
CMD ["node", "app.js"]
# CMD pm2-docker --json app.yml
